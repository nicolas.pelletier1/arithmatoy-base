#pragma once

#include <stddef.h>
#include <string.h>

extern int VERBOSE;

const char *get_all_digits();
extern const size_t ALL_DIGIT_COUNT;

enum operators 
{
    ADD,
    SUB,
    MUL
};


char *get_result_string(const char *lhs, const char *rhs, enum operators operator);

unsigned int get_digit_value(char base);

char to_digit(unsigned int value);

char *reverse(char *str);

const char *drop_leading_zeros(const char *number);

void debug_abort(const char *debug_msg);

size_t max(size_t nb1, size_t nb2);

size_t min(size_t nb1, size_t nb2);

char *add_zero(char *res, size_t num_zero, size_t len_res);

char *zeros_str(size_t zero_num);

char *mul_by_one_digit_factor(unsigned int base, unsigned int factor_value, const char *num, size_t len_num, char *zeros_to_add);
