#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"

#define DIGIT(digit, end_num) (digit == '\0' || end_num) ? '0' : digit

int VERBOSE = 1;


const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;


void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {

    if (VERBOSE) {
        fprintf(stderr, "add: entering function\n");
    }

    size_t len_lhs = strlen(drop_leading_zeros(lhs));
    size_t len_rhs = strlen(drop_leading_zeros(rhs));

    char *rev_lhs = reverse(strdup(drop_leading_zeros(lhs)));
    char *rev_rhs = reverse(strdup(drop_leading_zeros(rhs)));

    if (len_lhs < len_rhs) 
    {
	    rev_lhs = rev_rhs;
	    rev_rhs = reverse(strdup(drop_leading_zeros(lhs)));;
    }
    char *result_str = get_result_string(rev_lhs, rev_rhs, ADD);
    size_t max_digit = max(len_lhs, len_rhs);
    size_t min_digit = min(len_lhs, len_rhs);
    unsigned res = 0;
    unsigned carry = 0;
    size_t i = 0;
    for (; i < max_digit; i++)
    {
        if (VERBOSE)
        {
            fprintf(stderr, "add: digit %c digit %c carry %d\n",
                    DIGIT(rev_lhs[i], i >= max_digit), DIGIT(rev_rhs[i], i >= min_digit), carry);
        }
	if (i > min_digit)
        	res = get_digit_value(rev_lhs[i]) + 0 + carry;
	else
        	res = get_digit_value(rev_lhs[i]) + get_digit_value(rev_rhs[i]) + carry;
        carry = (res >= base) ? 1 : 0;
        result_str[i] = to_digit(res % base);
        if (VERBOSE)
            fprintf(stderr, "add: result: digit %c carry %d\n", result_str[i], carry);
    }
    if (carry == 1)
    {
        fprintf(stderr, "add: final carry %d\n", carry);
        result_str[i] = to_digit(1);
    }

    return reverse(result_str);

    // Fill the function, the goal is to compute lhs + rhs
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
        fprintf(stderr, "sub: entering function\n");
    }
    char *rev_lhs = reverse(strdup(drop_leading_zeros(lhs)));
    char *rev_rhs = reverse(strdup(drop_leading_zeros(rhs)));

    char *result_str = get_result_string(rev_lhs, rev_rhs, SUB);
    size_t max_digit = max(strlen(rev_lhs), strlen(rev_rhs));
    size_t min_digit = min(strlen(rev_lhs), strlen(rev_rhs));

    //unsigned res = 0;
    unsigned carry = 0;
    size_t i = 0;
    int res = 0;
    for (; i < max_digit; i++)
    {
        if (VERBOSE)
        {
            fprintf(stderr, "sub: digit %c digit %c carry %d\n",
                    DIGIT(rev_lhs[i], 0), DIGIT(rev_rhs[i], i >= min_digit), carry);
        }
	
	if (i > min_digit)
        	res = get_digit_value(rev_lhs[i]) - carry;
	else
        	res = get_digit_value(rev_lhs[i]) - (get_digit_value(rev_rhs[i]) + carry);
	
        if (res < 0)
        {
            carry = 1;
            res += base; 
        }
        else
        {
            carry = 0;
        }
        result_str[i] = to_digit(res);
        if (VERBOSE)
            fprintf(stderr, "sub: result: digit %c carry %d\n", result_str[i], carry);
    }
    if (carry == 1)
	return NULL;
    return drop_leading_zeros(reverse(result_str));
    // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result
}

char *mul_by_one_digit_factor(unsigned int base, unsigned int factor_value, const char *num, size_t len_num, char *zeros_to_add) {

    ssize_t i = len_num - 1;
    char digit_fac = to_digit(factor_value);

    unsigned carry = 0;
    unsigned res = 0;

    char *result = calloc(len_num + strlen(zeros_to_add) + 2, sizeof(char));
    while (i >= 0)
    {
        if (VERBOSE)
            fprintf(stderr, "mul: digit %c digit %c carry %d\n", digit_fac, num[i], carry);

        res = factor_value * get_digit_value(num[i]) + carry;
        result[i] = to_digit(res % base);
        carry = res / base;
        if (VERBOSE)
            fprintf(stderr, "mul: result: digit %c carry %d\n", result[i], carry);
        i--;
    }
    if (carry != 0)
    {
    	char *result_tmp = strdup(result);
    	result[0] = to_digit(carry);
	strncpy(result + 1, result_tmp, strlen(result_tmp)); 
    }
    strncpy(result + strlen(result), zeros_to_add, strlen(zeros_to_add)); 
    
    if (carry && VERBOSE)
        fprintf(stderr, "mul: final carry %c\n", to_digit(carry));
    return result;
}



char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE)
        fprintf(stderr, "mul: entering function\n");
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);

    size_t len_rhs = strlen(rhs);
    size_t len_lhs = strlen(lhs);

    if (len_lhs < len_rhs) 
    {
	    const char *lhs_tmp = lhs;
	    size_t len_lhs_tmp = len_lhs;
	    lhs = rhs;
	    rhs = lhs_tmp;
	    len_lhs = len_rhs;
	    len_rhs = len_lhs_tmp;
    }

    char *result = get_result_string(lhs, rhs, MUL);
    ssize_t i = len_rhs - 1;

    while (i >= 0)
    {
        if (VERBOSE)
            fprintf(stderr, "mul: digit %c number %s\n", rhs[i], lhs);

        char *res = mul_by_one_digit_factor(base, get_digit_value(rhs[i]), lhs, len_lhs, zeros_str((len_rhs - 1) - i));
        if (result[i] == '\0')
            fprintf(stderr, "mul: add %s + %s\n", "0", res);
        else
            fprintf(stderr, "mul: add %s + %s\n", result, res);
        result = arithmatoy_add(base, result, res);
    	fprintf(stderr, "mul: result: %s\n", result);
        i--;
    }
    return result;
    // Fill the function, the goal is to compute lhs * rhs
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result
}





// Here are some utility functions that might be helpful to implement add, sub
// and mul:

char *zeros_str(size_t zero_num)
{
    char *zeros_str = calloc(zero_num + 1, sizeof(char));
    for (size_t i = 0; i < zero_num; i++)
        zeros_str[i] = '0';
    return zeros_str;
}

size_t max(size_t nb1, size_t nb2)
{
    return (nb1 > nb2) ? nb1 : nb2;
}

size_t min(size_t nb1, size_t nb2)
{
    return (nb1 < nb2) ? nb1 : nb2;
}



char *get_result_string(const char *lhs, const char *rhs, enum operators operator)
{
    size_t max_size_str = 0;
    if (operator == ADD)
        max_size_str += max(strlen(lhs), strlen(rhs)) + 1;
    else if (operator == SUB)
        max_size_str += max(strlen(lhs), strlen(rhs));
    else
        max_size_str += strlen(lhs) + strlen(rhs) + 2;
    char *result_str = calloc(max_size_str + 1, sizeof(char));
    if (!result_str)
        debug_abort("in get_result_string calloc return NULL");
    return result_str;
}

unsigned int get_digit_value(char digit) {
    // Convert a digit from get_all_digits() to its integer value
    if (digit == '\0')
        return 0;

    if (digit >= '0' && digit <= '9') {
        return digit - '0';
    }
    if (digit >= 'a' && digit <= 'z') {
        return 10 + (digit - 'a');
    }
    return -1;
}

char to_digit(unsigned int value) {
    // Convert an integer value to a digit from get_all_digits()
    if (value >= ALL_DIGIT_COUNT) {
        debug_abort("Invalid value for to_digit()");
        return 0;
    }
    return get_all_digits()[value];
}

char *reverse(char *str) {
    // Reverse a string in place, return the pointer for convenience
    // Might be helpful if you fill your char* buffer from left to right
    if (!str)
        debug_abort("reverse(str): Invalid value for str (null)");

    const size_t length = strlen(str);
    const size_t bound = length / 2;
    for (size_t i = 0; i < bound; ++i) {
        char tmp = str[i];
        const size_t mirror = length - i - 1;
        str[i] = str[mirror];
        str[mirror] = tmp;
    }
    return str;
}

const char *drop_leading_zeros(const char *number) {
    // If the number has leading zeros, return a pointer past these zeros
    // Might be helpful to avoid computing a result with leading zeros
    if (*number == '\0') {
        return number;
    }
    while (*number == '0') {
        ++number;
    }
    if (*number == '\0') {
        --number;
    }
    return number;
}

void debug_abort(const char *debug_msg) {
    // Print a message and exit
    fprintf(stderr, debug_msg);
    exit(EXIT_FAILURE);
}
